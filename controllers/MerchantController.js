const Merchant = require('../models/Merchant')
const Product = require('../models/Product')
const User = require('../models/User')
const bcrypt = require('bcrypt')
const auth = require('../auth')
const Order = require('../models/Order')

// Merchant Registration
module.exports.registration = async (data) => {
	let emailExist = await User.find({email: data.email}).then((result) => {
		if(result.length > 0){
			return true
		}

		return false
		
	})

	if(emailExist === false) {
		let encrypted_password = bcrypt.hashSync(data.password, 10)

		let new_merchant = await new Merchant(
			{
				firstName: data.firstName,
				lastName: data.lastName,
				email: data.email,
				password: encrypted_password,
				mobileNo: data.mobileNo,
				isMerchant: true
			}
		) 
		return new_merchant.save().then((created_merchant, error) => {
			if(error){
				return error
			}

			return {
				message: 'A merchant account has been created.'
			}
		})
	}

	return {
		message: 'An account with the same email already exists.'
	}
}

// Retrieve Merchant Order/s
// module.exports.retrieveOrders = (request) => {
// 	let token = request.headers.authorization
// 	let merchantData = auth.decode(token)
// 	let merchantId = merchantData.id

// 	return Order.find().then((result) => {
// 		console.log(merchantId)
// 	})
// }
