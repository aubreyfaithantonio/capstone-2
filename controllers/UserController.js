const User = require('../models/User')
const bcrypt = require('bcrypt')
const auth = require('../auth')
const Order = require('../models/Order')

// User Registration(*)
module.exports.register = async (data) => {
	let emailExist = await User.find({email: data.email}).then((result) => {
		if(result.length > 0){
			return true
		}
		return false
	})

	if(emailExist === false) {
		let encrypted_password = bcrypt.hashSync(data.password, 10)

		let new_user = await new User(
			{
				firstName: data.firstName,
				lastName: data.lastName,
				email: data.email,
				password: encrypted_password,
				mobileNo: data.mobileNo,
				isMerchant: data.isMerchant
			}
		) 
		return new_user.save().then((created_user, error) => {
			if(error){
				return {
					success: false,
					error: error,
					message: 'Something went wrong. Please try again later'
				}
			}

			return {
				success: true,
				message: 'Your account has been created.'
			}
		})
	}

	return {
		success: false,
		message: 'An account with the same email already exists.'
	}
}

// User Authentication(*)
module.exports.login = (data) => {
	return User.findOne({email: data.email}).then((result) => {
		if(result == null){
			return {
				success: false,
				message: "The user does not exist."
			}
		} 

		const isPasswordCorrect = bcrypt.compareSync(data.password, result.password)
		if(isPasswordCorrect == true){
			let user_level = 'user'
			if (result.isAdmin === true && result.isMerchant === false) {
				user_level = 'admin'
			} else if (result.isAdmin === false && result.isMerchant === true) {
				user_level = 'merchant'
			}
			return{
				success: true,
				id: result._id,
				level: user_level,
				access: auth.createAccessToken(result)
			}
		}

		return {
			success: false,
			message: 'The email and password you have entered does not match our records. Please double-check and try again.'
		}
	})
}

// Retrieve specific user details(*)
module.exports.getUserDetails = (user_id) => {
	return User.findById(user_id, {password: 0}).then((user_id_details, error) => {
		if(error){
			return error
		}
		return user_id_details
	})
}

// User Orders
module.exports.getOrders = (request) => {
	let token = request.headers.authorization
	let userData = auth.decode(token)
	let userId = userData.id

	return Order.find({userId: userId}).then((result) => {
		return result
	})
}
