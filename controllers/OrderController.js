const Order = require('../models/Order')
const User = require('../models/User')
const Product = require('../models/Product')
const auth = require('../auth')
const Cart = require('../models/Cart')

// User Only - Checkout (*)
module.exports.checkout = async (request) => {
	let token = request.headers.authorization
	let userData = auth.decode(token)
	let cartId = request.params.cartId
	let subTotal = 0
	let total = 0
	let sf = 50
	let coupon = 20
	let cartDetails = await Cart.findOne({$and:[{_id: cartId}, {userId: userData.id}, {status: 'Pending'}]}).then((result) => {
		return result
	})

	if(cartDetails == null){
		return {
			message: 'Failed to retrieve cart details.'
		}
	}
	
	subTotal = cartDetails.subtotalAmount.reduce((total, value) => {
		return total + value
	})

	total = subTotal + sf

	total -= coupon

	let order = {
		userId: userData.id,
		products: cartDetails.products,
		totalAmount: total,
		status: "Completed"
	}

	cartDetails.remove()

	// Function to save data to database
	let productCheckout = await new Order(order)

	return productCheckout.save().then((checkedOutProducts, error) => {
		if(error){
			return error
		}

		return {
			message: 'Cart has been checked out successfully.'
		}
	})
}

// User Only - Get all Orders
module.exports.allOrders = (request) => {
	let token = request.headers.authorization
	let userData = auth.decode(token)
	let userId = userData.id
	
	return Orders.findOne({userId: userId}).then((result) => {
		return result
	})
}