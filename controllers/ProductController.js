const Product = require('../models/Product')
const auth = require('../auth')

// Get all ACTIVE products 
module.exports.getAvailableProducts = () => {
	return Product.find({isActive: true}).then((result) => {
		return result
	})
}

// Get products per category
module.exports.sortCategory = (request) => {
	let category = request.params.category
	return Product.find({$and:[{isActive: true}, {category: category}]}).then((result) => {
		return result
	})
}

// Get single product (For User)(*)
module.exports.getSpecificProduct = (request) => {
	let product_id = request.params.productId
	return Product.findById(product_id).then((result) => {
		return result
	})
}

// Create a product (Merchant only)(*)
module.exports.createProduct = (product, headers) => {
	let token = headers.authorization
	let merchant_data = auth.decode(token)
	let new_product = new Product({
		merchantId: merchant_data.id,
		category: product.category,
		name: product.name,
		description: product.description,
		price: product.price,
		stocks: product.stocks
	})

	return new_product.save().then((new_product, error) => 
	{
		if(error)
		{
			return false
		}

		return {
			message: 'New product successfully created.'
		}
	})
}

// Get all products (Merchant only)
module.exports.getAllProducts = (token) => {
	let merchant_data = auth.decode(token)
	return Product.find({merchantId: merchant_data.id}).then((result) => {
		return result
	})
}

// Get all ACTIVE products (Specific Merchant only)(*)
module.exports.getAllActiveProducts = (token) => {
	let merchant_data = auth.decode(token)
	return Product.find({merchantId: merchant_data.id,  isActive: true}).then((result) => {
		return result
	})
}

// Archive product (Specific Merchant only)(*)
module.exports.archiveOrUnarchiveProduct = (request) => {
	let token = request.headers.authorization
	let product_id = request.params.productId
	let update = {isActive: request.body.status}

	let merchant_data = auth.decode(token)

	return Product.findOneAndUpdate({$and:[{_id: product_id}, {merchantId: merchant_data.id}]}, update).then((archived_product, error) => {
		if(archived_product == null){
			return {
				message: "Archive failed"
			}
		}
		
		if(error){
			return false
		}

		if (request.body.status === false) {
			return {
				message: 'The product has been archived successfully.'
			}
		}
		
		return {
			message: 'The product has been unarchived successfully.'
		}
	})
}

// Get all ARCHIVE products (Specific Merchant only)
module.exports.getAllArchiveProducts = (token) => {
	let merchant_data = auth.decode(token)
	return Product.find({merchantId: merchant_data.id,  isActive: false}).then((result) => {
		return result
	})
}

// Update product info (Specific Merchant only)(*)
module.exports.updateProduct = (request) => {
	let token = request.headers.authorization
	let product_id = request.params.productId
	let merchant_data = auth.decode(token)
	let updated_product = request.body

	return Product.findOneAndUpdate({$and:[{_id: product_id}, {merchantId: merchant_data.id}, {isActive: true}]}, 
	{
		category: updated_product.category,
		name: updated_product.name,
		description: updated_product.description,
		price: updated_product.price,
		stocks: updated_product.stocks
	}

	).then((newProduct, error) => {
		if(newProduct == null){
			return {
				message: "Update failed"
			}
		}
		if(error) {
			return error
		}

		return {
			message: "The product is successfully updated"
		}
	})
}