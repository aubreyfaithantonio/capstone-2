const Order = require('../models/Order')
const User = require('../models/User')
const Product = require('../models/Product')
const Cart = require('../models/Cart')
const auth = require('../auth')

// Add product to cart
module.exports.addToCart = async (request) => {
	let token = request.headers.authorization
	let userData = auth.decode(token)
	let productId = request.body.productId
 	let productQuantity = request.body.quantity
	let userId = userData.id
	let subTotals = []

	let computeSubTotal = (productPrice, productQuantity) => {
		return productPrice*productQuantity
	}

	let productDetails = await Product.findById(productId).then((result) => {
		return result
	})

	let product_price = productDetails.price
	let product_name = productDetails.name
	let product_desc = productDetails.description


	let cartDetails = await Cart.findOne({userId: userId}).then((result) => {
		return result
	})

	// If there is no products and if the status is still pending, create a new instance of cart
	if(cartDetails == null){
		let cart = new Cart({
			userId: userId,
			products: [{
				productId: productId,
				productName: product_name,
				productPrice: product_price,
				quantity: productQuantity,
				productDescription: product_desc
			}],
			subtotalAmount: [computeSubTotal(product_price, productQuantity)]
		})

		return cart.save().then((savedCart, error) => {
			if(error){
				return error
			}

			return {
				message: 'Product is added to cart'
			}
		})
	}

	// Else, update the existing cart with the added product
	cartDetails.products.push({
		productId: productId,
		productName: product_name,
		productPrice: product_price,
		quantity: productQuantity,
		productDescription: product_desc
	})

 	cartDetails.subtotalAmount.push(computeSubTotal(product_price, productQuantity))

	return cartDetails.save().then((addedProduct, error) => {
		if(error){
			return error
		}

		return {
			message: 'Product is added to an existing cart'
		}
	})
}

// Get cart
module.exports.getCart = (request) => {
	let token = request.headers.authorization
	let userData = auth.decode(token)
	let userId = userData.id
	
	return Cart.findOne({userId: userId}).then((result) => {
		return result
	})
}

// Update product quantity in cart
module.exports.updateCart = async (request) => {
	let token = request.headers.authorization
	let userData = auth.decode(token)
	let userId = userData.id
	let productId = request.body.productId
	let productQuantity = request.body.quantity
	let cartId = request.body.cartId

	let cartDetails = await Cart.findOne({$and:[{_id: cartId}, {userId: userId}]}).then((result) => {
		return result
	})


	if (cartDetails === null) {
		return {
			message: "Please make sure you are updating your own cart."
		}
	}

	for(let i = 0; i < cartDetails.products.length; i++){
		if(cartDetails.products[i].productId == productId) {
			let initialSubTotal = cartDetails.subtotalAmount[i]
			let initialQuantity = cartDetails.products[i].quantity
			let productPrice = initialSubTotal/initialQuantity

			console.log(initialSubTotal, initialQuantity, productPrice)

			cartDetails.products[i].quantity = productQuantity
			cartDetails.subtotalAmount[i] = productQuantity*productPrice
		}
	}

	return cartDetails.save().then((addedProduct, error) => {
		if(error){
			return error
		}

		return {
			message: "Product quantity is succesfully updated"
		}
	})
}

// Remove product from cart
module.exports.removeProduct = async (request) => {
	let token = request.headers.authorization
	let userData = auth.decode(token)
	let userId = userData.id
	let productId = request.body.productId
	let cartId = request.body.cartId

	let cartDetails = await Cart.findOne({$and:[{_id: cartId}, {userId: userId}]}).then((result) => {
		return result
	})

	if (cartDetails === null) {
		return {
			message: "Please make sure you are removing products from your own cart."
		}
	}

	for(let i = 0; i < cartDetails.products.length; i++){
		if(cartDetails.products[i].productId == productId){
			cartDetails.products.splice(i, 1)
			cartDetails.subtotalAmount.splice(i, 1)
		}
	}
	return cartDetails.save().then((addedProduct, error) => {
		if(error){
			return error
		}

		return {
			message: "Product is successfully removed from cart"
		}
	})
}
