const Merchant = require('../models/Merchant')
const Product = require('../models/Product')
const User = require('../models/User')
const bcrypt = require('bcrypt')
const auth = require('../auth')

// Change User to Merchant
module.exports.userToMerchant = async (user_id) => {
	return User.findById(user_id).then((user, error) => {
		if(error){
			return error
		}
		if(user == null){
			return {
				message: 'User not found'
			}
		}
		if(user.isMerchant == true){
			return{
				message: 'User is already a merchant.'
			}
		}
		user.isMerchant = true
		return user.save().then((userIsMerchant, error) => {
			if(error){
				return error
			}

			return userIsMerchant
		})
	})
}

// Get all Users
module.exports.getAllUsers = (token) => {
	let admin_data = auth.decode(token)
	return User.find({$and:[{isAdmin: false}, {isMerchant: false}]}).then((result) => {
		return result
	})
}

// Get all Merchants
module.exports.getAllMerchants = (token) => {
	let admin_data = auth.decode(token)
	return User.find({$and:[{isAdmin: false}, {isMerchant: true}]}).then((result) => {
		return result
	})
}

// Get all Products
module.exports.getAllProducts = (token) => {
	let admin_data = auth.decode(token)
	return Product.find({}).then((result) => {
		return result
	})
}