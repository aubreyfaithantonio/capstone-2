const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')
const dotenv = require('dotenv')

dotenv.config()

const userRoutes = require('./routes/userRoutes')
const productRoutes = require('./routes/productRoutes')
const orderRoutes = require('./routes/orderRoutes')
const merchantRoutes = require('./routes/merchantRoutes')
const cartRoutes = require('./routes/cartRoutes')
const adminRoutes = require('./routes/adminRoutes')

const app = express()
const port = 2800

// MongoDB Connection
mongoose.connect(`mongodb+srv://aubreyfaithantonio:${process.env.MONGODB_PW}@cluster0.qkdjh4x.mongodb.net/backend-capstone3?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection
db.on('open', () => console.log('You are connected to MongoDB.'))
db.on('error', () => console.log('Failed to connect to database'))
// MongoDB Connection END

app.options('*', cors())
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({extended: true}))

// Routes
app.use('/users', userRoutes)
app.use('/products', productRoutes)
app.use('/orders', orderRoutes)
app.use('/merchant', merchantRoutes)
app.use('/cart', cartRoutes)
app.use('/admin', adminRoutes)
app.use('/', (request, response) => {
	response.send('Welcome to the homepage')
})
// Routes END

app.listen(process.env.PORT || port, () => {
	console.log(`API is now running on localhost:${process.env.PORT || port}`)
})