const express = require('express')
const router = express.Router()
const AdminController = require('../controllers/AdminController')
const auth = require('../auth')

// User to Merchant function
router.patch('/changetoMerchant/:id', auth.isAdmin, (request, response) => {
	AdminController.userToMerchant(request.params.id).then((result) => {
		response.send(result)
	})
})

// Get all Users
router.get('/allUsers', auth.isAdmin, (request, response) => {
	AdminController.getAllUsers(request.headers.authorization).then((result) => {
		response.send(result)
	})
})
// Get all Merchants
router.get('/allMerchants', auth.isAdmin, (request, response) => {
	AdminController.getAllMerchants(request.headers.authorization).then((result) => {
		response.send(result)
	})
})
// Get all Products - archived or not
router.get('/allProducts', auth.isAdmin, (request, response) => {
	AdminController.getAllProducts(request.headers.authorization).then((result) => {
		response.send(result)
	})
})

module.exports = router