const express = require('express')
const router = express.Router()
const UserController = require('../controllers/UserController')
const auth = require('../auth')

// User Registration
router.post('/register', (request, response) => {
	UserController.register(request.body).then((result) => {
		response.send(result)
	})
})

// User Authentication
router.post('/login', (request, response) => {
	UserController.login(request.body).then((result) => {
		response.send(result)
	})
})

// Retrieve User details
router.get("/:id/profile", auth.verify, (request, response) => {
	UserController.getUserDetails(request.params.id).then((result) => {
		response.send(result)
	})
})

// User Orders
router.get('/orders', auth.isUser, (request, response) => {
	UserController.getOrders(request).then((result) => {
		response.send(result)
	})
})

module.exports = router