const express = require('express')
const router = express.Router()
const MerchantController = require('../controllers/MerchantController')
const auth = require('../auth')

// Merchant Registration
router.post('/registration', auth.isAdmin, (request, response) => {
	MerchantController.registration(request.body).then((result) => {
		response.send(result)
	})
})

// Retrieve Merchant Order/s
router.get('/orders', auth.isMerchant, (request, response) => {
	MerchantController.retrieveOrders(request).then((result) => {
		response.send(result)
	})
})

module.exports = router