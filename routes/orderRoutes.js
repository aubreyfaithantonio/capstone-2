const express = require('express')
const router = express.Router()
const OrderController = require('../controllers/OrderController')
const auth = require('../auth')

// User Checkout - can be used for single and multiple checkout
router.post('/checkout/:cartId', auth.isUser, (request, response) => {
	OrderController.checkout(request).then((result) => {
		response.send(result)
	})
})

// Get all Orders (User)
router.get('/all', auth.isUser, (request, response) => {
	OrderController.checkout(request).then((result) => {
		response.send(result)
	})
})

module.exports = router