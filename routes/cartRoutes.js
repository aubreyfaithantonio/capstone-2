const express = require('express')
const router = express.Router()
const cartController = require('../controllers/CartController')
const auth = require('../auth')

// Only a USER can use and manipulate data in Cart

// Add product to cart
router.post('/add', auth.isUser, (request, response) => {
	cartController.addToCart(request).then((result) => {
		response.send(result)
	})
})
// Update product in cart
router.patch('/update', auth.isUser, (request, response) => {
	cartController.updateCart(request).then((result) => {
		response.send(result)
	})
})
// Remove product/s in cart
router.delete('/removeProduct', auth.isUser, (request, response) => {
	cartController.removeProduct(request).then((result) => {
		response.send(result)
	})
})

// Get cart
router.get('/get', auth.isUser, (request, response) => {
	cartController.getCart(request).then((result) => {
		response.send(result)
	})
})
module.exports = router