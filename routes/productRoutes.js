const express = require('express')
const router = express.Router()
const ProductController = require('../controllers/ProductController')
const auth = require('../auth')

// Get all ACTIVE products (User only)
router.get('/all-available', (request, response) => {
	ProductController.getAvailableProducts().then((result) => {
		response.send(result)
	})
})

// Get all products (Merchant only)
router.get('/all-products', auth.isMerchant, (request, response) => {
	ProductController.getAllProducts(request.headers.authorization).then((result) => {
		response.send(result)
	})
})

// Get all active products (Merchant only)
router.get('/all-active', auth.isMerchant, (request, response) => {
	ProductController.getAllActiveProducts(request.headers.authorization).then((result) => {
		response.send(result)
	})
})

//Get Specific Category (User)
router.get('/:category', (request, response) => {
	ProductController.sortCategory(request).then((result) => {
		response.send(result)
	})
})


// Get all products (Merchant only)
router.get('/all-products', auth.isMerchant, (request, response) => {
	ProductController.getAllProducts(request.headers.authorization).then((result) => {
		response.send(result)
	})
})

// Get single product (User only)
router.get('/:productId', (request, response) => {
	ProductController.getSpecificProduct(request).then((result) => {
		response.send(result)
	})
})

// Create a product (Merchant only)
router.post('/create', auth.isMerchant, (request, response) => {
	ProductController.createProduct(request.body, request.headers).then((result) => {
		response.send(result)
	})
})

// Archive product (Merchant only)
router.patch('/toggle/:productId', auth.isMerchant, (request, response) => {
	ProductController.archiveOrUnarchiveProduct(request).then((result) => {
		response.send(result)
	})
})

// Get all ARCHIVE products (Merchant only)
router.get('/all-archive', auth.isMerchant, (request, response) => {
	ProductController.getAllArchiveProducts(request.headers.authorization).then((result) => {
		response.send(result)
	})
})

// Update product info (Specific Merchant only)
router.patch('/update/:productId', auth.isMerchant, (request, response) => {
	ProductController.updateProduct(request).then((result) => {
		response.send(result)
	})
})

module.exports = router