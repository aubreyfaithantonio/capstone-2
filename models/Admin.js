const mongoose = require('mongoose')

const admin_schema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, 'First Name is required.']
	},
	lastName: {
		type: String,
		required: [true, 'Last Name is required.']
	},
	email: {
		type: String,
		required: [true, 'Email is required.']
	},
	password: {
		type: String,
		min: [6, 'Password too short.'],
		required: [true, 'Password is required.']
	},
	mobileNo: {
		type: String,
		required: [true, 'Mobile number is required.']
	},
	isAdmin: {
		type: Boolean,
		default: true
	},
	isMerchant: {
		type: Boolean,
		default: false
	}
})

module.exports = mongoose.model('Admin', admin_schema)