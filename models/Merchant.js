const mongoose = require('mongoose')

const merchant_schema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, 'First Name is required.']
	},
	lastName: {
		type: String,
		required: [true, 'Last Name is required.']
	},
	email: {
		type: String,
		required: [true, 'Email is required.']
	},
	password: {
		type: String,
		required: [true, 'Password is required.']
	},
	mobileNo: {
		type: String,
		required: [true, 'Mobile number is required.']
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	isMerchant: {
		type: Boolean,
		default: true
	}
}, { timestamps: true })

module.exports = mongoose.model('Merchant', merchant_schema)