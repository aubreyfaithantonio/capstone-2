const mongoose = require('mongoose')

const order_schema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, 'User ID is required.']
	},
	products: [
		{
			productId: {
				type: String,
				required: [true, 'Product ID is required.']
			},
			productName: {
				type: String,
				required: [true, 'Product Name is required.']
			},
			productDescription: {
				type: String,
				default: ''
			},
			productPrice: {
				type: Number,
				required: [true, 'Product Price is required.']
			},
			quantity: {
				type: Number,
				required: [true, 'Quantity is required.']
			}
		}
	],
	totalAmount: {
		type: Number,
		required: [true, '']
	},
	status: {
		type: String,
		default: 'Pending',
		enum: ['Pending', 'Paid', 'Completed']
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	}
})

module.exports = mongoose.model('Order', order_schema)