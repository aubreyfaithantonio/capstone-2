const mongoose = require('mongoose')

const product_schema = new mongoose.Schema({
	merchantId: {
		type: String,
		required: [true, 'User ID is required.']
	},
	category: {
		type: String,
		enum: ['Drinks & Beverages', 'Desserts & Snacks', 'Cooking Essentials', 'Health & Beauty', 'Household Supplies', 'Cellphone Load', 'Miscellaneous'],
		required: [true, 'Product category is required.']
	},
	name: {
		type: String,
		required: [true, 'Product name is required.']
	},
	description: {
		type: String,
		required: [true, 'Product description is required.']
	},
	price: {
		type: Number,
		required: [true, 'Price is required.']
	},
	stocks: {
		type: Number,
		required: [true, 'Stocks is required.']
	},
	isActive: {
		type: Boolean,
		default: true
	}

}, { timestamps: true })

module.exports = mongoose.model('Product', product_schema)