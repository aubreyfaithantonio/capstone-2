const mongoose = require('mongoose')

const cart_schema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, 'User ID is required.']
	},
	products: [
		{
			productId: {
				type: String,
				required: [true, 'Product ID is required.']
			},
			productName: {
				type: String,
				required: [true, 'Product Name is required.']
			},
			productDescription: {
				type: String,
				default: ''
			},
			productPrice: {
				type: Number,
				required: [true, 'Product Price is required.']
			},
			quantity: {
				type: Number,
				required: [true, 'Quantity is required.']
			}
		}
	],
	subtotalAmount: {
		type: Array,
		default: []
	}
	
}, {timestamps: true})

module.exports = mongoose.model('Cart', cart_schema)