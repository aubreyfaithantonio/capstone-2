const jwt = require('jsonwebtoken')
const secret = "ECommerceAPI"

module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin,
		isMerchant: user.isMerchant
	}

	return jwt.sign(data, secret, {expiresIn: '3h'})
}

module.exports.verify = (request, response, next) => {
	let token = request.headers.authorization

	if(typeof token !== "undefined"){

		token = token.slice(7, token.length)

		return jwt.verify(token, secret, (error, data) => {
			if(error){
				return response.send({
					auth: "Session expired. Please login again."
				})
			}
			else {
				next()
			}
		})
	}

	else {
		return response.send({
			auth: "Failed"
		})
	}
}

module.exports.decode = (token) => {
	if(typeof token !== "undefined"){

		token = token.slice(7, token.length)

		return jwt.verify(token, secret, (error, data) => {
			if(error){
				return error
			}
			else{
				return jwt.decode(token, {complete:true}).payload
			}
		})
	}

	else {
		return null
	}
}

// Merchant Verification
module.exports.isMerchant = (request, response, next) => {
	let token = request.headers.authorization

	if(typeof token !== "undefined"){

		token = token.slice(7, token.length)

		return jwt.verify(token, secret, (error, data) => {
			if(error){
				return response.send({
					auth: "Session expired. Please login again."
				})
			}
			if(data.isMerchant !== true){
				return response.send({
					auth: "You can't use this feature right now."
				})
			}

			next()
			
		})
	}

	else {
		return response.send({
			auth: "Failed"
		})
	}
}

// User Verification
module.exports.isUser = (request, response, next) => {
	let token = request.headers.authorization

	if(typeof token !== "undefined"){

		token = token.slice(7, token.length)

		return jwt.verify(token, secret, (error, data) => {
			if(error){
				return response.send({
					auth: "Session expired. Please login again."
				})
			}
			if(data.isMerchant !== false || data.isAdmin !== false){
				return response.send({
					auth: "You can't use this feature right now."
				})
			}

			next()
			
		})
	}

	else {
		return response.send({
			auth: "Failed"
		})
	}
}

// Admin Verification
module.exports.isAdmin = (request, response, next) => {
	let token = request.headers.authorization

	if(typeof token !== "undefined"){

		token = token.slice(7, token.length)

		return jwt.verify(token, secret, (error, data) => {
			if(error){
				return response.send({
					auth: "Session expired. Please login again."
				})
			}
			if(data.isAdmin !== true){
				return response.send({
					auth: "You can't use this feature right now."
				})
			}

			next()
			
		})
	}

	else {
		return response.send({
			auth: "Failed"
		})
	}
}